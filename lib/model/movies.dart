import 'package:dataclass/dataclass.dart';
import 'package:flutter/cupertino.dart';

/// Created by LieuNV on 9/7/2020
class Movies {
  String name;
  final int id;
  @Collection(deepEquality: true) // Short-hand: @Collection()
  final List<String> part;

  // Movies(this.name, this.id, this.part);
  Movies({@required this.id, this.name = "Default", this.part});

  @override
  String toString() {
    return 'Movies: $id - $name';
  }

  void _doingSomething() {
    print('Hello everyone...');
    this.handleEvent();
  }

  void sayHello() {
    _doingSomething();
  }

  Function handleEvent;
}

var s1 = 2;
String s2 = "Hello";
var s3 = '$s1 - $s2';
var s4 = s3 + s2;
List<int> number = [1, 2, 30];
List<int> numbers;
List<String> numberString = number.map((e) => '$e').toList();
int numberString2 = number.first;

void main(List<String> args) {
  number.forEach((element) {
    print(element);
  });
  numberString.forEach((element) {
    element.length;
    element.substring(0, 1);
    element.contains('D');
  });

  var movies = Movies(id: 1);

  List<Movies> movies2 = <Movies>[
    Movies(id: 3, name: 'Hello'),
    Movies(
      id: 1,
    ),
    Movies(id: 2)
  ];
  movies2.sort((movies1, movies3) => movies1.id - movies3.id);
  movies2.sort((movies1, movies3) => movies3.id - movies1.id);
  movies2.sort((movies1, movies3) => movies1.name.compareTo(movies3.name));
  movies2.add(Movies(id: 6, name: 'Paditech'));
  movies2[0].name = 'dsad';
  movies2.forEach((element) {
    print(element);
  });

  // filter in list
  var filterMovies = movies2
      .where((element) =>
          element.name == 'Hello' && element.name.toUpperCase().contains('H'))
      .toList();

  // delete an item using filter
  movies2 = movies2.where((element) => element.name != 'Hello').toList();
  movies2.forEach((element) {
    print('List Movies new: ' + element.toString());
  });
  // get list name in list movies
  List<String> names = movies2.map((e) => e.name).toList();
  names.forEach((element) {
    print('List name : $element');
  });
  print(filterMovies.toString());

  // 'const' and 'final' in primative value is like
  const number2 = 2;
  final number3 = 2;

  final List<int> someNumber = [1, 2, 3, 4, 5];
  someNumber.add(
      1); // you can add more item to a 'final' list.In 'const' list, you canot do that!
  someNumber[1] =
      8; // you can update item's value in list final.In 'const' list, you canot do that!
  someNumber.forEach((element) {
    print(element);
  });
  //reference to final list
  List<int> someNumber2 = someNumber;
  print('Refernece list:');
  someNumber2.forEach((element) {
    print(element);
  });

  //Map
  Map<String, String> map = new Map();
  map["doing"] = 'something';
  Map<String, Object> map2 = new Map(); //dynamic and Object
  map2['java'] = 2;
  map2['C++'] = "C++";
}
