import 'package:dataclass/dataclass.dart';

class PersonModel{
  final String name;
  final String price;
  @Collection(deepEquality: true)  // Short-hand: @Collection()
  final List<String> part ;

  PersonModel(this.name, this.price, this.part);
}