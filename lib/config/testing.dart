import 'package:flutter/material.dart';
import 'package:flutter_demo/env.dart';

void main() => Testing();

class Testing extends Env {
  final String baseUrl = 'https://api.testing.website.org';
  final Color primarySwatch = Colors.lime;
}
